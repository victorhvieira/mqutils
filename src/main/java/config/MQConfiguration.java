package config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MQConfiguration {
	// to load application's properties, we use this class
	private static Properties mainProperties = null;

	private MQConfiguration() {
	}

	public static String getString(String key) {
		try {
			return mainProperties.getProperty(key);
		} catch (Exception e) {
			return '!' + key + '!';
		}
	}

	public static void initializeProperties(InputStream is) {
		Properties properties = new Properties();
		// load all the properties from this file
		try {
			properties.load(is);
		} catch (IOException ioex) {
			System.out.println("ERROR initializing configuration.properties. Is the file present?");
		}
		// if no error initialize
		mainProperties = properties;
	}
}
