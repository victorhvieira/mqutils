import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import config.MQConfiguration;
import mq.utils.MQManagerUtils;

/**
 * Copied from main example demonstrating Apache Commons CLI. Apache Commons CLI
 * and more details on it are available at http://commons.apache.org/cli/.
 * 
 * @author Victor
 */
public class MQUtilsCLI {
	private static Options options = new Options();
	private static final String FILE_NAME = "configuration.properties"; //$NON-NLS-1$
	/**
	 * Apply Apache Commons CLI PosixParser to command-line arguments.
	 * 
	 * @param commandLineArguments
	 *            Command-line arguments to be processed with Posix-style
	 *            parser.
	 * @throws Exception
	 */
	public static void usePosixParser(final String[] commandLineArguments) throws Exception {
		final CommandLineParser cmdLinePosixParser = new PosixParser();
		final Options posixOptions = constructPosixOptions();
		CommandLine commandLine;
		try {
			commandLine = cmdLinePosixParser.parse(posixOptions, commandLineArguments);
			if (commandLine.hasOption("display")) {
				System.out.println(
						"Connect to the queue: " + MQManagerUtils.getQueueConnection().getName());
				System.out.println("Number of messages in the queue: "
						+ MQManagerUtils.countMessagesFromQueue(MQManagerUtils.getQueueConnection()));
			}
			if (commandLine.hasOption("put")) {
				MQManagerUtils.sendMessageToQueue(MQManagerUtils.getQueueConnection());
			}
			if (commandLine.hasOption("setup")) {
				executeMQSetupForMC();
			}
			if (commandLine.hasOption("clean")) {
				MQManagerUtils.cleanMessagesFromQueue(MQManagerUtils.getQueueConnection());
			}
			if (commandLine.hasOption("exit")) {
				MQManagerUtils.disconnect();
				System.exit(0);
			}
		} catch (ParseException parseException) // checked exception
		{
			System.err
					.println("Encountered exception while parsing using PosixParser:\n" + parseException.getMessage());
		}
	}

	/**
	 * Construct and provide Posix-compatible Options.
	 * 
	 * @return Options expected from command-line of Posix form.
	 */
	public static Options constructPosixOptions() {
		final Options posixOptions = new Options();
		posixOptions.addOption("s", "setup", false, "Setup local MQ environment")
				.addOption("display", false, "Display the queue state.")
				.addOption("p", "put", false, "Put message in queue").addOption("c", "clean", false, "Clean queue")
				.addOption("e", "exit", false, "Close connection and exit");
		return posixOptions;
	}

	/**
	 * Display command-line arguments without processing them in any further
	 * way.
	 * 
	 * @param commandLineArguments
	 *            Command-line arguments to be displayed.
	 */
	public static void displayProvidedCommandLineArguments(final String[] commandLineArguments,
			final OutputStream out) {
		final StringBuffer buffer = new StringBuffer();
		for (final String argument : commandLineArguments) {
			buffer.append(argument).append(" ");
		}
		try {
			out.write((buffer.toString() + "\n").getBytes());
		} catch (IOException ioEx) {
			System.err.println("WARNING: Exception encountered trying to write to OutputStream:\n" + ioEx.getMessage());
			System.out.println(buffer.toString());
		}
	}

	/**
	 * Display example application header.
	 * 
	 * @out OutputStream to which header should be written.
	 */
	public static void displayHeader(final OutputStream out) {
		final String header = "[MQ Utils CLI for Mastercard EIB] \n";
		try {
			out.write(header.getBytes());
		} catch (IOException ioEx) {
			System.out.println(header);
		}
	}

	/**
	 * Write the provided number of blank lines to the provided OutputStream.
	 * 
	 * @param numberBlankLines
	 *            Number of blank lines to write.
	 * @param out
	 *            OutputStream to which to write the blank lines.
	 */
	public static void displayBlankLines(final int numberBlankLines, final OutputStream out) {
		try {
			for (int i = 0; i < numberBlankLines; ++i) {
				out.write("\n".getBytes());
			}
		} catch (IOException ioEx) {
			for (int i = 0; i < numberBlankLines; ++i) {
				System.out.println();
			}
		}
	}

	/**
	 * Print usage information to provided OutputStream.
	 * 
	 * @param applicationName
	 *            Name of application to list in usage.
	 * @param options
	 *            Command-line options to be part of usage.
	 * @param out
	 *            OutputStream to which to write the usage information.
	 */
	public static void printUsage(final String applicationName, final Options options, final OutputStream out) {
		final PrintWriter writer = new PrintWriter(out);
		final HelpFormatter usageFormatter = new HelpFormatter();
		usageFormatter.printUsage(writer, 80, applicationName, options);
		writer.flush();
	}

	/**
	 * Write "help" to the provided OutputStream.
	 */
	public static void printHelp(final Options options, final int printedRowWidth, final String header,
			final String footer, final int spacesBeforeOption, final int spacesBeforeOptionDescription,
			final boolean displayUsage, final OutputStream out) {
		final String commandLineSyntax = "./runMQUtils.sh";
		final PrintWriter writer = new PrintWriter(out);
		final HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp(writer, printedRowWidth, commandLineSyntax, header, options, spacesBeforeOption,
				spacesBeforeOptionDescription, footer, displayUsage);
		writer.flush();
	}

	/**
	 * Main executable method used to demonstrate Apache Commons CLI.
	 * 
	 * @param commandLineArguments
	 *            Commmand-line arguments.
	 * @throws Exception
	 */
	public static void main(final String[] commandLineArguments) {
		final String applicationName = "./runMQUtils.sh";
		InputStream configFile;
		try {
			configFile = new FileInputStream(FILE_NAME);
		} catch (FileNotFoundException fex) {
			configFile = MQUtilsCLI.class.getResourceAsStream(FILE_NAME);
		} 
		if(configFile != null) {
			MQConfiguration.initializeProperties(configFile);
			try {
				configFile.close();
			} catch (IOException e) {
				//ignore it
			}
		} else {
			System.out.println("ERROR initializing configuration.properties. Is the file present?");
			System.exit(-1);;
		}
		displayHeader(System.out);
		displayBlankLines(1, System.out);
		if (commandLineArguments.length < 1) {
			System.out.println("-- USAGE --");
			printUsage(applicationName + " (Posix)", constructPosixOptions(), System.out);
			displayBlankLines(2, System.out);

			System.out.println("-- HELP --");
			printHelp(constructPosixOptions(), 80, "COMMAND HELP", "End of Help", 3, 5, true, System.out);
		}
		displayProvidedCommandLineArguments(commandLineArguments, System.out);
		try {
			usePosixParser(commandLineArguments);
		} catch (Exception e) {
			System.out.println("PROGRAM EXECUTED WITH ERROR! PLEASE CHECK CONFIGURATION AND TRY AGAIN!");
		}
		// useGnuParser(commandLineArguments);
	}

	private static void executeMQSetupForMC() {
		String queueManager = MQConfiguration.getString("mqserver.queuemanager"); //$NON-NLS-1$
		String queueName = MQConfiguration.getString("mqserver.queuename"); //$NON-NLS-1$
		try {
			String[] commands = new String[] { "sudo su mqm",
					"/opt/mqm/bin/setmqaut -m %QUEUE_MANAGER% -t q -n SYSTEM.MQEXPLORER.REPLY.MODEL -g rsksys +inq +browse +get +dsp",
					"/opt/mqm/bin/setmqaut -m %QUEUE_MANAGER% -t q -n SYSTEM.ADMIN.COMMAND.QUEUE -g rsksys +inq +browse +dsp +get +put",
					"/opt/mqm/bin/setmqaut -m %QUEUE_MANAGER% -t qmgr -g rsksys +all",
					"/opt/mqm/bin/setmqaut -m %QUEUE_MANAGER% -t q -n %QUEUE_NAME% -g rsksys +inq +browse +dsp +get +put" };
			executeCommandsInSequence(replaceInCommands(commands, queueManager, queueName));
			String[] alterCharsetCommands = new String[] {"/opt/mqm/bin/setmqaut/runmqsc " + queueManager, "ALTER QMGR CCSID (1208)", "END"};
			executeCommands(alterCharsetCommands);
		} catch (Exception e) {
			System.out.println("-- ERROR ON MQ SETUP UP --");
			e.printStackTrace();
		}		
	}

	private static String[] replaceInCommands(String[] commands, String queueManager, String queueName) {
		for (int i = 0; i < commands.length; i++) {
			try {
				String command = commands[i].replaceAll("%QUEUE_MANAGER%", queueManager);
				commands[i] = command.replaceAll("%QUEUE_NAME%", queueName);
			
			} catch (Exception e) {
				System.out.println("ERROR WHILE EXECUTING COMMAND: " + commands[0]);
				System.out.println(e.getMessage());
			}
		}
		
		return commands;
	}
	
	private static void executeCommandsInSequence(String[] commands) {
			try {
				ProcessBuilder procBuilder = new ProcessBuilder(commands);
				procBuilder.start();
			} catch (Exception e) {
				System.out.println("ERROR WHILE EXECUTING COMMANDS: " + commands);
				System.out.println(e.getMessage());
			}
	}
	


	private static void executeCommands(String[] commands) throws IOException, InterruptedException {
		Process process = Runtime.getRuntime().exec(commands);
		if (process.waitFor() > 0)
			System.out.println("COMMAND FAILED (WARNING): " + commands.toString());
		else
			System.out.println("COMMAND EXECUTED OK: " + commands.toString());
	}
}