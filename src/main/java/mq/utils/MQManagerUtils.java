package mq.utils;

import java.io.IOException;
import java.util.Hashtable;
import java.util.logging.Logger;

import com.ibm.mq.MQC;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.MQConstants;

import config.MQConfiguration;

public class MQManagerUtils {

	private static String host = MQConfiguration.getString("mqserver.host"); //$NON-NLS-1$
	private static int port = Integer.parseInt(MQConfiguration.getString("mqserver.port"));
	private static String queueManager = MQConfiguration.getString("mqserver.queuemanager"); //$NON-NLS-1$
	private static String channel = MQConfiguration.getString("mqserver.channelname"); //$NON-NLS-1$
	private static String queueName = MQConfiguration.getString("mqserver.queuename"); //$NON-NLS-1$
	private static String message = MQConfiguration.getString("mqserver.putmessage"); //$NON-NLS-1$
	private static MQQueue queueConnection;
	private static MQQueueManager connection;

	private static Logger LOGGER = Logger.getLogger("" + MQManagerUtils.class);

	public static void main(String[] args) throws Exception {
		MQManagerUtils.cleanMessagesFromQueue(getQueueConnection());
	}

	public static MQQueue getQueueConnection() {
		if (queueConnection == null) {
			// getting MQ connection
			connection = getConnectionFactory();
			try {
				// MQOO_OUTPUT = Open the queue to put messages. The queue is
				// opened for use with subsequent MQPUT calls.
				// MQOO_INPUT_AS_Q_DEF = Open the queue to get messages using
				// the queue-defined default.
				// The queue is opened for use with subsequent MQGET calls. The
				// type of access is either
				// shared or exclusive, depending on the value of the
				// DefInputOpenOption queue attribute.
				int openOptions = MQConstants.MQOO_OUTPUT | MQConstants.MQOO_INPUT_AS_Q_DEF;

				// creating destination
				LOGGER.info(String.format("Will attempt to access queue: %s", queueName)); //$NON-NLS-1$
				queueConnection = getConnectionFactory().accessQueue(queueName, openOptions);
			} catch (MQException e) {
				LOGGER.info(String.format("Error connecting when try to send  " //$NON-NLS-1$
						+ "message %s to queue %s. Ex: %s", message, queueName, e.getMessage())); //$NON-NLS-1$
			}
		}

		return queueConnection;
	}

	public static void sendMessageToQueue(MQQueue queue) {
		try {
			// specify the message options...
			MQPutMessageOptions pmo = new MQPutMessageOptions(); // default

			// MQPMO_ASYNC_RESPONSE = The MQPMO_ASYNC_RESPONSE option requests
			// that an MQPUT or MQPUT1 operation
			// is completed without the application waiting for the queue
			// manager to complete the call.
			// Using this option can improve messaging performance, particularly
			// for applications using client bindings.
			pmo.options = MQConstants.MQPMO_ASYNC_RESPONSE;
			LOGGER.info(String.format("Connected to queue, will put message in async way")); //$NON-NLS-1$
			queue.put(createMessage(message), pmo);
			System.out.println(String.format("Messaage put in the queue: %s", message)); //$NON-NLS-1$
		} catch (MQException e) {
			LOGGER.info(String.format("Error connecting when try to send  " //$NON-NLS-1$
					+ "message %s to queue %s. Exception: %s", message, queueName, e.getMessage())); //$NON-NLS-1$
		} catch (IOException e) {
			LOGGER.info(String.format("IOException found in method. Exception: %s", e)); //$NON-NLS-1$
		}
	}

	public static void cleanMessagesFromQueue(MQQueue queue) throws MQException {
		LOGGER.info(String.format("MQRead is now connected.\n")); //$NON-NLS-1$

		MQGetMessageOptions getOptions = new MQGetMessageOptions();
		getOptions.options = MQC.MQGMO_NO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQC.MQGMO_CONVERT;

		while (true) {
			MQMessage message = new MQMessage();
			try {
				queue.get(message, getOptions);
				byte[] b = new byte[message.getMessageLength()];
				message.readFully(b);
				LOGGER.info( String.format("Message read: %s ", new String(b)) );
				message.clearMessage();
			} catch (IOException e) {
				LOGGER.info(String.format("IOException during GET: %s " + e.getMessage())); //$NON-NLS-1$
				break;
			} catch (MQException e) {
				if (e.completionCode == 2 && e.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) {
					System.out.println(String.format("All messages removed from the queue: %s", queue.getName() )); //$NON-NLS-1$
				} else {
					LOGGER.info(String.format("GET Exception: %s", e.getMessage())); //$NON-NLS-1$
				}
				break;
			}
		}
	}
	
	public static int countMessagesFromQueue(MQQueue queue) throws MQException {
		int count = 0;
		
		MQGetMessageOptions getOptions = new MQGetMessageOptions();
		getOptions.options = MQC.MQGMO_NO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQC.MQGMO_CONVERT;

		while (true) {
			MQMessage message = new MQMessage();
			try {
				queue.get(message, getOptions);
				byte[] b = new byte[message.getMessageLength()];
				message.readFully(b);
				count++;
			} catch (IOException e) {
				LOGGER.info(String.format("IOException during GET: %s " + e.getMessage())); //$NON-NLS-1$
				break;
			} catch (MQException e) {
				break;
			}
		}
		return count;
	}


	private static MQQueueManager getConnectionFactory() {
		// Create a connection to the queue manager
		Hashtable<String, Object> props = new Hashtable<String, Object>();
		props.put(MQConstants.CHANNEL_PROPERTY, channel);
		props.put(MQConstants.PORT_PROPERTY, port);
		props.put(MQConstants.HOST_NAME_PROPERTY, host);
		props.put(MQConstants.USER_ID_PROPERTY, MQConfiguration.getString("mqserver.username")); //$NON-NLS-1$
		props.put(MQConstants.PASSWORD_PROPERTY, MQConfiguration.getString("mqserver.password")); //$NON-NLS-1$
		props.put(MQConstants.PASSWORD_PROPERTY, MQConfiguration.getString("mqserver.password")); //$NON-NLS-1$

		MQQueueManager qMgr = null;
		try {
			long t1 = System.currentTimeMillis();
			LOGGER.info(String.format("Attempting to connecting to MQ server with properties " //$NON-NLS-1$
					+ "%s and QueueManager %s", props, queueManager)); //$NON-NLS-1$
			qMgr = new MQQueueManager(queueManager, props);
			// MQSimpleConnectionManager()
			long t2 = System.currentTimeMillis();
			LOGGER.info(String.format("Connection OK, time spent in ms:  %d", (t2 - t1))); //$NON-NLS-1$
		} catch (MQException e) {
			LOGGER.info(String.format("Error connecting to MQ server with properties " //$NON-NLS-1$
					+ "%s and QueueManager %s. Error: %s", props, queueManager, e.getMessage())); //$NON-NLS-1$
		}

		return qMgr;
	}

	private static MQMessage createMessage(String message) throws IOException {
		LOGGER.info(String.format("Will create message object with content: %s ", message)); //$NON-NLS-1$
		// create message
		MQMessage mqMessage = new MQMessage();
		mqMessage.clearMessage();
		mqMessage.format = MQConstants.MQ_JMS_MSG_CLASS_BYTES;
		mqMessage.writeString(message);
		LOGGER.info(String.format("Message object created: %s ", mqMessage)); //$NON-NLS-1$
		return mqMessage;
	}

	public static void disconnect() {
		try {
			LOGGER.info(String.format("Will attempt to close access queue: %s", queueName)); //$NON-NLS-1$
			queueConnection.close();
			connection.disconnect();
			LOGGER.info(String.format("Connection to queue %s closed", queueName)); //$NON-NLS-1$
		} catch (MQException e) {
			LOGGER.info(String.format("Error when diconnecting from MQ Server: %s", e.getMessage())); //$NON-NLS-1$
		}
	}
}
